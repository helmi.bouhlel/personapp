import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/internal/operators';

@Injectable({
    providedIn: "root",
  })
  export class PersonService {
    constructor(private http: HttpClient) {}
    /**
     * Get All Person request.
     */
    getAllPerson(): Observable<any> {
      return this.http
        .get<any>("http://localhost:4200/person", { observe: "response" })
        .pipe(
          retry(3), // retry a failed request up to 3 times
          catchError(this.handleError) // then handle the error
        );
    }

    getPersonById(id : String): Observable<any> {
      return this.http
        .get<any>("http://localhost:4200/person/" +id, { observe: "response" })
        .pipe(
          retry(3), // retry a failed request up to 3 times
          catchError(this.handleError) // then handle the error
        );
    }

    /**
     * Add a new person post requts.
     * @param person a new person to add.
     */
    addPerson(person: any): Observable<any> {
      return this.http
        .post<any>("http://localhost:4200/person", person, {
          observe: "response",
        })
        .pipe(catchError(this.handleError));
    }


    editPerson(person: any): Observable<any> {
      return this.http
        .put<any>("http://localhost:4200/person", person, {
          observe: "response",
        })
        .pipe(catchError(this.handleError));
    }

    /**
     * Delete an person request method.
     * @param empId person unique id
     */
    deletePerson(empId: any) {
      return this.http
        .delete<any>("http://localhost:4200/person/" + empId, {
          observe: "response",
        })
        .pipe(catchError(this.handleError));
    }
    private handleError(error: HttpErrorResponse) {
      if (error.error instanceof ErrorEvent) {
        // A client-side or network error occurred. Handle it accordingly.
        console.error("An error occurred:", error.error.message);
      } else {
        // The backend returned an unsuccessful response code.
        // The response body may contain clues as to what went wrong,
        console.error(
          `Backend returned code ${error.status}, ` + `body was: ${error.error}`
        );
      }
      // return an observable with a user-facing error message
      return throwError("Something bad happened; please try again later.");
    }
  }