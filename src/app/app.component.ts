import { Component } from '@angular/core';
import { IPerson, Person } from './model/person.model';
import { PersonService } from './service/person.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = "http-interceptor-fakebackend";
  personList: IPerson[];
  per: IPerson;
  selectedPerson : IPerson;
  showLoading = false;
  showUpdateBtn = false;
  constructor(private personService: PersonService) {}
  
  ngOnInit(): void {
    this.per = new Person();
    this.getAllPersonFromServer();
  }

//#region  actions

 /**
   * Display Action
   * @param persId person unique id
   */
  onDisplayAction(pers: any) {
    this.selectedPerson = pers;
  }

   /**
   * Update Action
   * @param persId person unique id
   */
  onUpdatePersonAction(pers: any) {
    this.selectedPerson = null;
    this.updatePersonFromServer(pers);
  }

  /**
   * Delete Action
   * @param persId person unique id
   */
  onDeleteAction(persId: any) {
    this.deletePersonFromServer(persId);
  }

  onSelect(persone: any) {
    this.per = persone;
    this.selectedPerson = null;
    this.showUpdateBtn = true;
  }
  

  /**
   * Create a new person object and send to the server to save.
   */
  onAddPersonAction() { 
    // validate inputs
    if (!this.isValid(this.per)) return;
    this.per.id =  this.personList.reduce((acc, shot) => acc = acc > shot.id ? acc : shot.id, 0).valueOf();
    // add person
    this.addPersonToServer(this.per);
  }
  //#endregion actions
  //#region  Communication between fake backend and UI methods.
  
  updatePersonFromServer(person : IPerson) {
    this.showLoading = true;
    this.personService.editPerson(person).subscribe(
      (resp) => {
        if (resp.status == 200) {
          const deletedEmpId = resp.body;
          // delete from array.
          this.personList = this.personList.filter((f) => f.id !== deletedEmpId);
          this.showUpdateBtn = false;
          this.per = {};
        }
      },
      (err) => console.error("Error Occured When Delete An Person " + err),
      () => (this.showLoading = false)
    );
  }



  displayPersonFromServer(persId: any) {
    this.showLoading = true;
    this.showLoading = true;
    this.personService.getPersonById(persId).subscribe(
      (resp) => {
        if (resp.status == 200) {
          this.per = resp.body
        }
      },
      (err) => console.error("Error Occured When Add A New Person " + err),
      () => (this.showLoading = false) // close spinner
    );
  }
  
  
  /**
   * Add a new person object to server with http post.
   * @param newPerson 
   */
  addPersonToServer(pers: IPerson) {
    // show spinner
    this.showLoading = true;
    this.personService.addPerson(pers).subscribe(
      (resp) => {
        if (resp.status == 200) {
          // add to list
          this.personList.push(resp.body);
          this.per = {}
        }
      },
      (err) => console.error("Error Occured When Add A New Person " + err),
      () => (this.showLoading = false) // close spinner
    );
  }
  /**
   * Get All Person from local .json file for a first time.
   */
  getAllPersonFromServer() {
    this.showLoading = true;
    this.personService.getAllPerson().subscribe(
      (resp) => {
        if (resp.status == 200) {
          this.personList = resp.body;
          console.log(this.personList);
        }
      },
      (err) => console.error("Error Occured When Get All Person " + err),
      () => (this.showLoading = false)
    );
  }
  /**
   * Delete an person from server with given person uuid.
   * @param persId user public uuid
   */
  deletePersonFromServer(persId: any) {
    this.showLoading = true;
    this.personService.deletePerson(persId).subscribe(
      (resp) => {
        if (resp.status == 200) {
          const deletedEmpId = resp.body;
          // delete from array.
          this.personList = this.personList.filter((f) => f.id !== deletedEmpId);
        }
      },
      (err) => console.error("Error Occured When Delete An Person " + err),
      () => (this.showLoading = false)
    );
  }
  //#endregion  Communication between fake backend and UI methods.
  /**
   * Validate person object taken from form.
   * @param emp 
   */
  isValid(pers: any): boolean {
    console.log(pers)
    if (
      pers.firstName == undefined ||
      pers.lastName == undefined ||
      pers.email == undefined
    ) {
      alert("Please Enter Full Name");
      return false;
    }
    return true;
  }
}
