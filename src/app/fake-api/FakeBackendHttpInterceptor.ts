import { HttpClient, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { from, Observable, of } from 'rxjs';
import { delay } from 'rxjs/internal/operators';
import { v1 as uuidv4 } from 'uuid';

@Injectable()
export class FakeBackendHttpInterceptor implements HttpInterceptor {
  // default person json path
  private _personJsonPath = "assets/person.json";
  constructor(private http: HttpClient) {}
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return this.handleRequests(req, next);
  }
  /**
   * Handle request's and support with mock data.
   * @param req
   * @param next
   */
  handleRequests(req: HttpRequest<any>, next: HttpHandler): any {
    const { url, method } = req;
    if (url.endsWith("/person") && method === "GET") {
      req = req.clone({
        url: this._personJsonPath,
      });
      return next.handle(req).pipe(delay(500));
    }
    if (url.endsWith("/person") && method === "POST") {
      const { body } = req.clone();
      // assign a new uuid to new person
      body.id = uuidv4();
      return of(new HttpResponse({ status: 200, body })).pipe(delay(500));
    }
    if (url.endsWith("/person") && method === "PUT") {
      const { body } = req.clone();
      // assign a new uuid to new person
      body.id = uuidv4();
      return of(new HttpResponse({ status: 200, body })).pipe(delay(500));
    }
    if (url.match(/\/person\/.*/) && method === "DELETE") {
      const empId = this.getPersonId(url);
      return of(new HttpResponse({ status: 200, body: empId })).pipe(
        delay(500)
      );
    }
    
    // if there is not any matches return default request.
    return next.handle(req);
  }
  /**
   * Get Person unique uuid from url.
   * @param url
   */
  getPersonId(url: any) {
    const urlValues = url.split("/");
    return urlValues[urlValues.length - 1];
  }
}
/**
 * Mock backend provider definition for app.module.ts provider.
 */
export let fakeBackendProvider = {
  provide: HTTP_INTERCEPTORS,
  useClass: FakeBackendHttpInterceptor,
  multi: true,
};